package com.mindx.coviddetermine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovidDetermineApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidDetermineApplication.class, args);
	}

}
