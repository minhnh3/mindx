package com.mindx.coviddetermine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mindx.coviddetermine.model.UserInfo;

public interface IUserInfoRepository extends JpaRepository<UserInfo, Long> {

}
