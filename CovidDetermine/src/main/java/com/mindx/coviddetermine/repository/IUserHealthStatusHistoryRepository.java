package com.mindx.coviddetermine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mindx.coviddetermine.model.UserHealthStatusHistory;

public interface IUserHealthStatusHistoryRepository extends JpaRepository<UserHealthStatusHistory, Long> {

}
