package com.mindx.coviddetermine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindx.coviddetermine.model.UserInfo;
import com.mindx.coviddetermine.repository.IUserInfoRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class UserInfoController {
	@Autowired
	IUserInfoRepository userInfoRepository;

	@GetMapping("/users")
	public ResponseEntity<Object> getAllUserInfo(@RequestParam(required = false) Integer page) {
		try {
			if (page == null) {
				return new ResponseEntity<>(userInfoRepository.findAll(), HttpStatus.OK);
			}

			return new ResponseEntity<>(userInfoRepository.findAll(PageRequest.of(page, 10)), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@GetMapping("/users/{id}")
	public ResponseEntity<Object> getUserInfoById(@PathVariable Long id) {
		try {
			if (!userInfoRepository.existsById(id)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<>(userInfoRepository.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@PostMapping("/users")
	public ResponseEntity<Object> createUserInfo(@RequestBody UserInfo cUser) {
		try {
			UserInfo newUser = new UserInfo();
			newUser.setName(cUser.getName().trim());
			newUser.setPermanentAddress(cUser.getPermanentAddress().trim());
			newUser.setCurrentLocation(cUser.getCurrentLocation().trim());

			return new ResponseEntity<>(null, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/users/{id}")
	public ResponseEntity<Object> updateUserInfo(@PathVariable Long id, @RequestBody UserInfo cUser) {
		try {
			if (!userInfoRepository.existsById(id)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			UserInfo updateUser = new UserInfo();
			updateUser.setName(cUser.getName().trim());
			updateUser.setPermanentAddress(cUser.getPermanentAddress().trim());
			updateUser.setCurrentLocation(cUser.getCurrentLocation().trim());

			return new ResponseEntity<>(null, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/users/{id}")
	public ResponseEntity<Object> deleteUserInfoById(@PathVariable Long id) {
		try {
			if (!userInfoRepository.existsById(id)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			userInfoRepository.deleteById(id);

			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/users")
	public ResponseEntity<Object> deleteAllUserInfo() {
		try {
			userInfoRepository.deleteAll();

			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

}
