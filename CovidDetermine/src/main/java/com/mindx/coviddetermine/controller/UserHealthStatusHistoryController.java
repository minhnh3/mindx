package com.mindx.coviddetermine.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mindx.coviddetermine.model.UserHealthStatusHistory;
import com.mindx.coviddetermine.repository.IUserHealthStatusHistoryRepository;
import com.mindx.coviddetermine.repository.IUserInfoRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class UserHealthStatusHistoryController {
	@Autowired
	IUserInfoRepository userInfoRepository;

	@Autowired
	IUserHealthStatusHistoryRepository userHealthStatusHistoryRepository;

	@GetMapping("/users/healths")
	public ResponseEntity<Object> getAllUserHealthStatus(@RequestParam(required = false) Integer page) {
		try {
			if (page == null) {
				return new ResponseEntity<>(userHealthStatusHistoryRepository.findAll(), HttpStatus.OK);
			}

			return new ResponseEntity<>(userHealthStatusHistoryRepository.findAll(PageRequest.of(page, 10)),
					HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@GetMapping("/users/healths/{healthstatusid}")
	public ResponseEntity<Object> getUserHealthStatusById(@PathVariable Long healthstatusid) {
		try {
			if (!userHealthStatusHistoryRepository.existsById(healthstatusid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			return new ResponseEntity<>(userHealthStatusHistoryRepository.findById(healthstatusid), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@PostMapping("/users/{userid}/healths")
	public ResponseEntity<Object> createUserHealthStatus(@PathVariable Long userid,
			@RequestBody UserHealthStatusHistory cUserHealthStatusHistory) {
		try {
			if (!userInfoRepository.existsById(userid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			UserHealthStatusHistory newUserHealthStatusHistory = new UserHealthStatusHistory();
			newUserHealthStatusHistory.setHistoryLocation(cUserHealthStatusHistory.getHistoryLocation().trim());
			newUserHealthStatusHistory.setHealthStatus(cUserHealthStatusHistory.getHealthStatus().trim());
			newUserHealthStatusHistory.setCreateDate(new Date());
			newUserHealthStatusHistory.setUserInfo(userInfoRepository.findById(userid).get());

			return new ResponseEntity<>(userHealthStatusHistoryRepository.save(newUserHealthStatusHistory),
					HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/users/healths/{healthstatusid}")
	public ResponseEntity<Object> updateUserHealthStatus(@PathVariable Long healthstatusid,
			@RequestBody UserHealthStatusHistory cUserHealthStatusHistory) {
		try {
			if (!userHealthStatusHistoryRepository.existsById(healthstatusid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			UserHealthStatusHistory newUserHealthStatusHistory = userHealthStatusHistoryRepository
					.findById(healthstatusid).get();
			newUserHealthStatusHistory.setHistoryLocation(cUserHealthStatusHistory.getHistoryLocation().trim());
			newUserHealthStatusHistory.setHealthStatus(cUserHealthStatusHistory.getHealthStatus().trim());
			newUserHealthStatusHistory.setCreateDate(new Date());

			return new ResponseEntity<>(null, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/users/healths/{healthstatusid}")
	public ResponseEntity<Object> deleteUserHealthStatusById(@PathVariable Long healthstatusid) {
		try {
			if (!userHealthStatusHistoryRepository.existsById(healthstatusid)) {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}

			userHealthStatusHistoryRepository.deleteById(healthstatusid);

			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

	@DeleteMapping("/users/healths")
	public ResponseEntity<Object> deleteAllUserHealthStatus() {
		try {
			userInfoRepository.deleteAll();

			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception details: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed: " + e.getCause().getCause().getMessage());
		}
	}

}
