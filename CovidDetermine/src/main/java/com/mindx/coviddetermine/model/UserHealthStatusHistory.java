package com.mindx.coviddetermine.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "user_health_status_history")
public class UserHealthStatusHistory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank(message = "History Location must be filled")
	@Column(nullable = false)
	private String historyLocation;

	@NotBlank(message = "Health Status must be filled")
	@Column(nullable = false)
	private String healthStatus;

	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(nullable = false, updatable = false)
	private Date createDate;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private UserInfo userInfo;

	public UserHealthStatusHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserHealthStatusHistory(Long id,
			@NotBlank(message = "History Location must be filled") String historyLocation,
			@NotBlank(message = "Health Status must be filled") String healthStatus, Date createDate,
			UserInfo userInfo) {
		super();
		this.id = id;
		this.historyLocation = historyLocation;
		this.healthStatus = healthStatus;
		this.createDate = createDate;
		this.userInfo = userInfo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHistoryLocation() {
		return historyLocation;
	}

	public void setHistoryLocation(String historyLocation) {
		this.historyLocation = historyLocation;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

}
