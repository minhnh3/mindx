package com.mindx.coviddetermine.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "user_info")
public class UserInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotBlank(message = "Name must be filled")
	@Column(nullable = false)
	private String name;

	@NotBlank(message = "Permanent Address must be filled")
	@Column(nullable = false)
	private String permanentAddress;

	@NotBlank(message = "Current Location must be filled")
	@Column(nullable = false)
	private String currentLocation;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "userInfo")
	private List<UserHealthStatusHistory> userHealthStatusHistory;

	public UserInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserInfo(Long id, @NotBlank(message = "Name must be filled") String name,
			@NotBlank(message = "Permanent Address must be filled") String permanentAddress,
			@NotBlank(message = "Current Location must be filled") String currentLocation,
			List<UserHealthStatusHistory> userHealthStatusHistory) {
		super();
		this.id = id;
		this.name = name;
		this.permanentAddress = permanentAddress;
		this.currentLocation = currentLocation;
		this.userHealthStatusHistory = userHealthStatusHistory;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}

	public List<UserHealthStatusHistory> getUserHealthStatusHistory() {
		return userHealthStatusHistory;
	}

	public void setUserHealthStatusHistory(List<UserHealthStatusHistory> userHealthStatusHistory) {
		this.userHealthStatusHistory = userHealthStatusHistory;
	}

}
