SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- -- ----------------------------
-- -- Records of hibernate_sequence
-- -- ----------------------------
UPDATE `hibernate_sequence` SET `next_val` = 300;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES (1, 'current_location 1', 'name 1', 'permanent_address 1');
INSERT INTO `user_info` VALUES (2, 'current_location 2', 'name 2', 'permanent_address 2');
INSERT INTO `user_info` VALUES (3, 'current_location 3', 'name 3', 'permanent_address 3');
INSERT INTO `user_info` VALUES (4, 'current_location 4', 'name 4', 'permanent_address 4');
INSERT INTO `user_info` VALUES (5, 'current_location 5', 'name 5', 'permanent_address 5');
INSERT INTO `user_info` VALUES (6, 'current_location 6', 'name 6', 'permanent_address 6');
INSERT INTO `user_info` VALUES (7, 'current_location 7', 'name 7', 'permanent_address 7');
INSERT INTO `user_info` VALUES (8, 'current_location 8', 'name 8', 'permanent_address 8');
INSERT INTO `user_info` VALUES (9, 'current_location 9', 'name 9', 'permanent_address 9');
INSERT INTO `user_info` VALUES (10, 'current_location 10', 'name 10', 'permanent_address 10');

-- ----------------------------
-- Records of user_health_status_history
-- ----------------------------
INSERT INTO `user_health_status_history` VALUES (1, '2021-10-01 07:00:00', 'unknown', 'history_location 1', 1);
INSERT INTO `user_health_status_history` VALUES (2, '2021-10-01 10:00:00', 'unknown', 'history_location 2', 1);
INSERT INTO `user_health_status_history` VALUES (3, '2021-10-02 15:00:00', 'unknown', 'history_location 3', 1);
INSERT INTO `user_health_status_history` VALUES (4, '2021-10-01 09:00:00', 'unknown', 'history_location 1', 2);
INSERT INTO `user_health_status_history` VALUES (5, '2021-10-02 11:00:00', 'unknown', 'history_location 2', 2);
INSERT INTO `user_health_status_history` VALUES (6, '2021-10-03 20:00:00', 'unknown', 'history_location 3', 3);

SET FOREIGN_KEY_CHECKS = 1;
